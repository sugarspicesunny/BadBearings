﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class P1_ctrlmvm : MonoBehaviour {

	public float moveSpeed;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.W)) {
			GetComponent<Transform> ().eulerAngles = new Vector3 (0, 0, 0);
		}

		if (Input.GetKey (KeyCode.W)) {
			/*
			 GetComponent is a function to get the transform component from unity which is the Transform component. Transform is a generic function, hence why it is located in between pointy braces. We always put parentheses after a function to receive an input.
			 Translate is a function to move the object. Parentheses after function where we create a Vector3 just meaning, (xyz) and inside parentheses in order we jot down the integers seperated by commas and then close the parentheses. After that we end
			 the line of code with a semi colon. That is just the way of c# syntax.
			 */
			GetComponent<Transform> ().Translate (new Vector3 (0, 0, moveSpeed) * Time.deltaTime, Space.World);
		}

		if (Input.GetKey (KeyCode.S)) {
			GetComponent<Transform> ().Translate (new Vector3 (0, 0, -moveSpeed)* Time.deltaTime, Space.World);
		}
		// when clicking e and s at the same time it slows down and pulls in one direction
		// 
		if (Input.GetKey (KeyCode.E)) {
			
			if (Input.GetKey (KeyCode.S)) {
			
				//GetComponent<Transform> ().Translate (new Vector3 (0, 0, 0.5) * Time.deltaTime);
			
			}
		} 
		else 
		{
			if (Input.GetKeyDown (KeyCode.S)) {

				GetComponent<Transform> ().eulerAngles = new Vector3 (0, 180, 0);
			}

		}
	}	
  //[type of output] [name of function] ([input])
	int AddNumber(int firstNumber, int secondNumber)
	{
		return firstNumber + secondNumber;
	}
}
