﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObjects : MonoBehaviour {

	public GameObject ebutton;

	void OnTriggerEnter (Collider other){
		// only when it's the movable tag
		// = : assigning a value into a variable; i.e. int number = 5;
		// == : checking is the value is the same as the variable; i.e. if (number == 5) {}
		if (other.gameObject.tag == "Movable" ) {

			ebutton.SetActive (true);
		}
	}

	void OnTriggerExit (Collider other){

		if (other.gameObject.tag == "Movable") {

			ebutton.SetActive (false);
		}

	}

	void OnTriggerStay (Collider other){
	

		if (other.gameObject.tag == "Movable") {
			
			if (Input.GetKey (KeyCode.E)) {
                print("gotit");
				// make the obstacle the child, make player_1 parent
				other.transform.parent = gameObject.transform;

				//Makes the GameObject "newParent" the parent of the GameObject "player".
				//player.transform.parent = newParent.transform;
		
			}

			if (Input.GetKeyUp (KeyCode.E)) {

				print (other. name);
				other.transform.parent = null;
			
			}

		}

	}


}
